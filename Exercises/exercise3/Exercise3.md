Good work displaying the extra repo info :)
Next we're going to add additional information to be displayed: a list of tags in the repository!

Display repository tag list
===========================

You will need to modify `TagListServlet` to fetch the Tags in a repository and pass it to the soy template via the
context map (hint: see how the repository object is passed through)

You will also need to modify the soy template to display the list of tags

*Hint: You can use the component `RepositoryMetadataService` to fetch repository tags.*

You can use this by adding the following component-import:

    <component-import key="repositoryMetadataService" interface="com.atlassian.stash.repository.RepositoryMetadataService"/>

Don't forget to have it injected into the constructor of `TagListServlet` so you can use it :)

Finally, its worth noting that most of Stash's services are paginated to avoid loading huge abouts of entities into
memory (and potentially running out of it). In order to use these APIs you'll need to construct a `PageRequest`. The
easiest way to do so is to use the default implementation `PageRequestImpl`

    new PageRequestImpl(0, 25)

This will create a page request starting from index 0 and will return at most a page of 25 entities.