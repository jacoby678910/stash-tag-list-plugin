(function($, AJS) {
    'use strict';

    var SCROLL_BUFFER = 40;

    var $document = $(document);

    $document.ready(function() {
        var $tagList = $('#tag-list');

        // Exercise 5 Answer: Retrieve information from the DOM
        var nextPageStart = parseInt($tagList.attr('data-next-page-start'), 10);
        var projectKey = $tagList.attr('data-project-key');
        var repoSlug = $tagList.attr('data-repo-slug');

        if (!nextPageStart) {
            return;
        }

        var $window = $(window);
        var nextPagePromise = null;

        function onScroll() {
            if (nextPagePromise) {
                return;
            }
            if (!nextPageStart) {
                // Exercise 5 Answer: Optimisation, unregister the listener when all pages are loaded
                $window.off('scroll', onScroll);
            }

            // Exercise 5 Answer: Calculate when the user is near the bottom, triggering a REST request when they are
            var scrollHeight = $document.height();
            var scrollPosition = $window.height() + $window.scrollTop();

            if ((scrollHeight - scrollPosition) <= SCROLL_BUFFER) {
                loadNextPage();
            }
        }

        function loadNextPage() {
            // Exercise 5 Answer: Call the Stash REST API for the next page
            nextPagePromise = $.ajax({
                type: 'GET',
                dataType: "json",
                url: AJS.contextPath() + '/rest/api/latest/projects/' + projectKey + '/repos/' + repoSlug + '/tags?start=' + nextPageStart
            });
            nextPagePromise.done(function(tags) {
                // Exercise 5 Answer: Save the nextPageStart to be used for second, third etc. pages
                nextPageStart = tags.nextPageStart;
                // Exercise 5 Answer: Call 'plugin.page.tags.tagRows' template and attach the results to the DOM
                $tagList.children('tbody').append(plugin.page.tags.tagRows({ tags: tags }));
            });
            nextPagePromise.always(function() {
                // Exercise 5 Answer: Prevents multiple requests to the server concurrently
                nextPagePromise = null;
            })
        }

        $window.on('scroll', onScroll);
    });
})(jQuery, AJS);