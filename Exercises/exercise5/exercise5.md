You should now have a pretty looking page which looks mostly like the rest of the application. However what happens
if there are more than 25 (or whatever page limit you chose) in the repository? We will need to fetch the next page of
tags using JavaScript and client side soy templates.


Rendering on the client
=======================

In order to render client side you are going to need to use both JavaScript and Soy. To do this, you need to add a
new `web-resource`. Add the following to your `atlassian-plugin.xml`

    <web-resource key="tag-list-resources">
        <!-- In order to render soy templates on the client, we need to compile them. -->
        <!-- This is done server-side via a web-resource transformer. The soy template -->
        <!-- can be invoked just like a JavaScript function under a namespace -->
        <transformation extension="soy">
            <transformer key="soyTransformer"/>
        </transformation>

        <!-- Contexts are a good way of grouping web-resources and are often used an an -->
        <!-- 'entry point' onto a particular page. You can include the context below -->
        <!-- on your page by invoking the webResourceManager_requireResourcesForContext() -->
        <!-- soy function on in your server-side template. -->
        <context>plugin.page.tags</context>

        <resource type="download" name="tag-list-common.soy.js" location="static/page/tag-list-common.soy"/>
        <resource type="download" name="tag-list.js" location="static/page/tag-list.js"/>

        <dependency>com.atlassian.auiplugin:ajs</dependency>
    </web-resource>

You can also go ahead and create the `tag-list-common.soy` file and `tag-list.js` file referenced above.

In your `tag-list-common.soy` file, try to create a template which renders just the table rows given a page of tags.
You can base your implementation off your existing code in `tag-list.soy`. You should also invoke this new template
when rendering rows on the server-side as well.

In your `tag-list.js` file you will want to render the next page of tags when the user scrolls to the bottom of the
page and continue to do this until you have reached the last page. Here is some code to get you started:

    (function($) {
        var $document = $(document);
        var $window = $(window);

        var SCROLL_BUFFER = 40;

        function onScroll() {
            var scrollHeight = $document.height();
            var scrollPosition = $window.height() + $window.scrollTop();

            if ((scrollHeight - scrollPosition) <= SCROLL_BUFFER) {
                // Load next page
            }
        }

        $window.on('scroll', onScroll);
    })(jQuery);

Below are some other JavaScript functions you will almost certainly need

* `AJS.contextPath()` - returns the current context path of the application. e.g. '/stash'
* `jQuery` - Can be used to manipulate the DOM. Refer to [API documentation](http://api.jquery.com/) for more information
* `jQuery.ajax` - Used to perform REST requests back to the server. Refer to the [API documentation](api.jquery.com/jQuery.ajax/) for more information

Luckily Stash already has a REST resource for retrieving pages of tags it is in the form:

    <BASE_URL>/rest/api/latest/projects/<PROJECT KEY>/repos/<REPO SLUG>/tags?start=<PAGE START>

You may need to pass some of the the information required to make this call from the server to the client code. One way
of doing this is via the DOM. e.g.

    <div id="param-info" data-param-from-server="{$someParam}" />

And then:

    var paramFromServer = $('#param-info').attr('data-param-from-server');

Good luck!