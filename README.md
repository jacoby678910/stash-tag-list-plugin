Overview
========

In this practical, we're going to create a basic plugin that adds a menu item to stash, which when selected, displays
a list of tags in the repository.


Plugin anatomy
==============

Explore the structure of the plugin, you'll notice that the plugin has:

* `src/main/java` - this is where your java code will live in. A basic servlet is implemented, which you will add to
  later on.

* `src/main/resources` - this is where your resources like images, javascript files, soy files etc will live.
  Most importantly, this contains the plugin descriptor: atlassian-plugin.xml. A basic soy template is provided here
  which you will modify later.

* `atlassian-plugin.xml` - contains plugin info and plugin modules. You'll find two plugin modules in there, a servlet
  and a web-item. We will be adding more plugin modules.

* `pom.xml` - used by Maven to manage dependencies and build configuration

* `LICENSE.txt` - The license file for the project. This project is licensed under BSD. Normally our software is either
  closed-source licensed under the Atlassian End User License Agreement (e.g. JIRA, Confluence) or open sourced under
  a BSD or MIT license which plays well with closed-source software.


Getting Started
===============

You can build the plugin by using the following command:

    mvn clean package

This will package your plugin as a JAR file write it to the target/ directory.

A JAR file is pretty useless without having a way to run it. To start up Stash with your plugin installed run:

    mvn stash:debug

After Stash starts up, you should be able to access it in your browser at http://localhost:7990/stash. You can login
using the username/password admin/admin or user/user.

The plugin is pretty basic. It currently just adds a custom page to our application. You can access the page at
http://localhost:7990/stash/plugins/servlet/tag-list/. Have a look at the `atlassian-plugin.xml` and the
`TagListServlet` class. Do you understand how its works?

One of the benefits of plugins is that you can reinstall them into the a running application. Try modifying the
text in `TagListServlet`. Then run the following command in a different terminal:

    mvn stash:cli

Once you get a new prompt, run the following command to install the new version of your plugin:

    pi

When you're comfortable with starting Stash and deploying the plugin, browse to `Exercises/exercise1` and start the 
first exercise.

What's next?
============

When you're ready, attempt the exercises! Answers are available for you to check against, or to copy/paste if you're
stuck. There is more exercises than you will be able to get through in the time allocated, so work at your own pace
and feel free to complete some of the later exercises in your spare time.